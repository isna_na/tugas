# Intent (Explicit dan Implicit)

Jika kalian sudah sampai pada materi ini maka pastinya kalian sudah dapat membuat aplikasi android sendiri bukan? namun apakah kalian menyadari bahwa aplikasi yang dapat kalian buat hanya memiliki satu halaman saja? ya, pada umumnya aplikasi memiliki lebih dari satu halaman bahkan aplikasi android dapat memiliki banyak halaman sekaligus. oleh karena itu kali ini kita akan belajar membuat halaman baru atau disebut dengan activity dan pada pembelajaran ini juga akan diajarkan cara untuk berpindah ke halaman lain.

Pada aplikasi android biasanya tidak hanya terdapat layar tampilan saja tapi terdapat lebih dari satu layar tampilan yang berarti ada lebih dari satu activity yang terdapat pada aplikasi tersebut. Untuk aplikasi yang menggunakan activity lebih, maka perlu adanya perpindahan activity yang muncul di layar. Untuk memindahkan activity kita dapat menggunakan Intent. 


Beberapa fungsi dari intent adalah :
1. Memulai Service
2. Launch activity
3. Display web page
4. Display contact list
5. Message broadcasting


-----------------------------------------------------------------------------------------------------------------------------------

## Pembuatan Activity

Untuk dapat menggunakan intent maka kita perlu membuat activity lain. Adapun cara untuk membuat activity lain adalah sebagai berikut.

1.  Klik File > New > Activity > Empty Activity.
2.  Pada configure activity isi Activity name sesuai keinginan dan gunakan source language java.
3.  Tekan Finish.
4.  Tunggu hingga selesai.

-----------------------------------------------------------------------------------------------------------------------------------

## Penggunaan Intent

Intent merupakan suatu Class dalam programming android yang digunakan untuk berpindah activity dan kita dapat membawa data ke activity lainya.

Untuk menggunakan intent bisa dilihat cara dibawah.

• Format penggunaan :

```java
Intent intent = new Intent(Context activity sekarang, Class activity tujuan);
startActivity(intent);
```

• Contoh Penggunaan : 

```java
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
        startActivity(intent);
    }
}
```
-----------------------------------------------------------------------------------------------------------------------------------

## Jenis Intent

### Terdapat 2 tipe intent, yaitu :

#### 1. Implicit Intent.
Yaitu intent yang berfungsi untuk memulai action pada aplikasi seperti membuka web, membuat sms, dan banyak lagi tanpa mengirim suatu data. Mudahnya Implicit intent adalah intent yang berintraksi dengan aplikasi lain.

• Berikut contoh penggunaanya : 

```java
String url = "www.google.com";
Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url));//membuka web
startActivity(intent);
```

• Macam macam Action pada intent : 


| Intent | Fungsi |
| ------ | ------ |
| Intent. ACTION_EDIT | Mengedit Kontak |
| Intent.ACTION_SET_ALARM | Membuat Alarm |
| Intent. ACTION_SET_TIMER | Membuat Timer |

> *Note : Intent.ACTION_VIEW memiliki banyak kegunaan tergantung dari parameter yang dibawanya.


#### 2. Explicit Intent

Yaitu intent yang berfungsi untuk berpindah activity dengan atau tanpa membawa data. Mudahnya Explicit intent adalah intent yang berintraksi dengan bagian dalam aplikasi seperti pemindahan data. Untuk penggunaanya sama seperti contoh pada penggunaan intent. untuk memindahkan data ke activity lain ikuti petunjuk berikut.

• Format :

**Pada Activity Asal**
```
intent.putExtra(Nama/key data(bertipe String), variable isi data);
```
> *Note : Nama/key bebas diisi sesuka user.

**Pada Activity Tujuan**
```
String sessionId = getIntent().getStringExtra(Nama/key data asal(bertipe String));
```

> *Note : Nama/key yang menerima harus sama dengan saat mengirim data.

• Contoh :

**Pada Activity Asal**
```java
Intent intent = new Intent(getBaseContext(), SignoutActivity.class);
intent.putExtra("EXTRA_SESSION_ID", sessionId); // mengirim data
startActivity(intent);
```

**Pada Activity Tujuan**
```java
String sessionId = getIntent().getStringExtra("EXTRA_SESSION_ID");
```

-----------------------------------------------------------------------------------------------------------------------------------

## Membuat Aplikasi Pembuka Website Sederhana

Setelah mempelajari intent, ada baiknya jika kita langsung mencoba agar materi yang kita pelajari dapat kita mengerti lebih baik. Untuk itu kita akan membuat aplikasi untuk membuka website.

• Berikut langkah membuat aplikasinya : 

1. Buat Project baru dengan package com.example.intent.
2. Pada main activity, ubah kode kalian menjadi seperti ini.

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity"
    android:orientation="vertical">

    <TextView
        android:layout_marginTop="100dp"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="10dp"
        android:text="Masukan Url Website : "
        android:textColor="#000000"
        android:textSize="30sp" />

    <EditText
        android:id="@+id/etMainInput"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="Link"
        android:textColor="#000000"
        android:textSize="30sp" />

    <Button
        android:id="@+id/btnMainLanjut"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Lanjut"
        android:textColor="#000000"
        />

</LinearLayout>
```

3. Selanjutnya buat activity baru dengan nama Detail.
4. Setelah activity baru selesai dibuat, buka activity_detail.xml pada directory res > layout > activity_detail.xml.
5. Jika sudah ubah kode yang ada pada activity detail menjadi seperti berikut.

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".Detail">

    <TextView
        android:layout_marginTop="100dp"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="10dp"
        android:text="Link Url Website : "
        android:textColor="#000000"
        android:textSize="30sp" />

    <TextView
        android:padding="10dp"
        android:id="@+id/textDetailLink"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Null"
        android:textColor="#000000"
        android:textSize="30sp" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        android:layout_gravity="center"
        android:gravity="right"
        >
        <Button
            android:id="@+id/btnDetailBack"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Back"
            android:textColor="#000000"
            />
        <ImageButton
            android:id="@+id/btnDetailLanjut"
            android:layout_width="100dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_baseline_search_24"
            android:text="Lanjut"
            android:textColor="#000000"
            />
    </LinearLayout>

</LinearLayout>
```

6. Jika sudah, sekarang kita akan mulai untuk mengurus integrasi java dan layout.
7. Buka MainActivity.java pada directory java > com.example.intent > MainActivity.java lalu ubah kode yang ada menjadi seperti  berikut

```java
package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText etInput;
    Button btnNext;
    protected String inputData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etInput = findViewById(R.id.etMainInput);
        btnNext = findViewById(R.id.btnMainLanjut);

        btnNext.setOnClickListener(new View.OnClickListener() {//jika button diklik
            @Override
            public void onClick(View view) {
                inputData = etInput.getText().toString();//mengambil data dari editext
                try {
                    Intent intent = new Intent(getApplicationContext(), Detail.class);
                    intent.putExtra("Url", inputData);//mengirimkan String inputData
                    startActivity(intent);//memulai intent(contoh Explicit)
                }catch (Exception e)//Jika Input User Kosong
                {
                    Toast.makeText(getApplicationContext(),"Url Kosong!", Toast.LENGTH_SHORT).show();//menampilkan pesan “Url Kosong!”
                }
            }
        });

    }
}
```

8. Jika sudah diubah, sekarang tinggal mengubah kode pada Detail.java yang ada pada dir yang sama dengan MainActivity.java.
9. Buka Detail.java, kemudian ubah kode menjadi seperti berikut.

```java
package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    TextView textLink;
    Button btnBack;
    ImageButton btnNext;
    String dataInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        textLink = findViewById(R.id.textDetailLink);
        btnBack = findViewById(R.id.btnDetailBack);
        btnNext = findViewById(R.id.btnDetailLanjut);
        Intent intentData = getIntent();
        dataInput = intentData.getStringExtra("Url");//mengambil data dengan nama “Url” lalu memasukanya ke variable dataInput.
        textLink.setText(dataInput);
        btnBack.setOnClickListener(new View.OnClickListener() {//saat btnBack diklik
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {//saat btnNext diklik
            @Override
            public void onClick(View view) {
                Intent intentWeb = new Intent(Intent.ACTION_VIEW);
                if (!dataInput.startsWith("http://") && !dataInput.startsWith("https://"))
                {//mengecek apakah input memiliki “http://”
                    dataInput = "http://" + dataInput;
                }//karena jika tidak ada maka akan error, coba saja kalau mau tau.
                intentWeb.setData(Uri.parse(dataInput));//memasukan url ke intentWeb
                startActivity(intentWeb);//memulai Intent(contoh Implicit)
            }
        });

    }
}
```

10. Jika sudah selesai program dapat dicoba dan dijalankan.
11. Hasil program seperti berikut.

![Contoh Hasil](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/intent_0.jpeg)
![Contoh Hasil](https://gitlab.com/isna_na/tugas/-/raw/master/intentEdit.jpeg)

12. Jika menekan tombol lanjut pada halaman kedua, maka browser akan terbuka kan menampilkan website yang ingin dituju.

![Contoh Hasil](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/intent_2.jpeg)

13. Aplikasi sudah selesai dibuat.
